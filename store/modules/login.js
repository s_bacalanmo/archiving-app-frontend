import { postApi } from '~/plugins/http'

const initialState = () => {
  return {
    user: {}
  }
}
const state = initialState()

const mutations = {
  SET_SAMPLE (state, payload) {
    state.navigation = payload
  },
  RESET_NAVIGATION_STATE (state) {
    /*
      FUNCTION:
      RESET TO INITIAL STATE. YOU WILL APPRECIATE THIS FUNCTIONALITY
      WHEN YOU ALREADY DEALING WITH COOKIE / LOCAL STORAGE
    */
    Object.assign(state, initialState())
  }
}

const getters = {
  user: state => state.user
}

const actions = {
  CHANGE_SAMPLE ({ commit }, payload) {
    commit('SET_SAMPLE', payload)
  },
  USER_REGISTRATION ({ commit }, payload) {
    return postApi('https://localhost:44315/api/AuthManagement/Register', payload)
  },
  LOGIN_USER ({ commit }, payload) {
    return postApi('https://localhost:44315/api/AuthManagement/Login', payload)
  }
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
