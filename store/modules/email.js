/* eslint-disable */

// import { postApi, deleteApi, patchApi } from '~/plugins/http'
// import _ from 'lodash'
// import Vue from 'vue'

const initialState = () => {
  return {
    getemail: {}
  }
}


const state = initialState()

const mutations = {
  SET_EMAIL (state, payload) {
    state.getemail = payload
  },
  RESET_EMAIL_STATE (state) {
    Object.assign(state, initialState())
  }
}

const getters = {
  getemail: state => state.getemail
}

const actions = {
  
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
