const initialState = () => {
  return {
    nav: false,
    dynamicToolbarName: ''
  }
}
const state = initialState()

const mutations = {
  SET_NAV (state, payload) {
    state.nav = payload
  },
  SET_DYNAMIC_TOOLBAR_NAME (state, payload) {
    state.dynamicToolbarName = payload
  },
  RESET_STOREEVENTS_STATE (state) {
    /*
      FUNCTION:
      RESET TO INITIAL STATE. YOU WILL APPRECIATE THIS FUNCTIONALITY
      WHEN YOU ALREADY DEALING WITH COOKIE / LOCAL STORAGE
    */
    Object.assign(state, initialState())
  }
}

const getters = {
  nav: state => state.nav,
  dynamicToolbarName: state => state.dynamicToolbarName
}

const actions = {

}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
