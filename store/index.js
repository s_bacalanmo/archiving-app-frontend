import Vue from 'vue'
import Vuex from 'vuex'

import sample from './modules/sample'
import email from './modules/email'
import storeevents from './modules/storeevents'
import login from './modules/login'

Vue.use(Vuex)

const store = () => {
  return new Vuex.Store({
    modules: {
      sample,
      login,
      email,
      storeevents
    }
  })
}

export default store
