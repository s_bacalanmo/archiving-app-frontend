import Vue from 'vue'

import Toolbar from '~/components/global/Toolbar'
import Navbar from '~/components/global/Navbar'
import Snackbar from '~/components/global/Snackbar'
import CustomSnackbar from '~/components/global/CustomSnackbar'

import GlobalDashboardCard from '~/components/dashboard/GlobalDashboardCard'
import UrgentEmails from '~/components/dashboard/UrgentEmails'
import AdvancedSearch from '~/components/dashboard/AdvancedSearch'

import ComposeForm from '~/components/documents/compose/ComposeForm'
import ComposeAttachments from '~/components/documents/compose/ComposeAttachments'
import ComposeLinks from '~/components/documents/compose/ComposeLinks'

import SentSearch from '~/components/documents/sent/SentSearch'
import SentList from '~/components/documents/sent/SentList'
import SentFilter from '~/components/documents/sent/SentFilter'
import SentPreview from '~/components/documents/sent/SentPreview'

import InboxSearch from '~/components/documents/inbox/InboxSearch'
import InboxList from '~/components/documents/inbox/InboxList'
import InboxFilter from '~/components/documents/inbox/InboxFilter'
import InboxPreview from '~/components/documents/inbox/InboxPreview'

import DraftSearch from '~/components/documents/drafts/DraftSearch'
import DraftList from '~/components/documents/drafts/DraftList'
import DraftFilter from '~/components/documents/drafts/DraftFilter'

import RouteForm from '~/components/documents/route/RouteForm'
import RouteAttachments from '~/components/documents/route/RouteAttachments'
import RouteLinks from '~/components/documents/route/RouteLinks'
import RouteComments from '~/components/documents/route/RouteComments'

import UserListSearch from '~/components/account/userlist/UserListSearch'
import UserList from '~/components/account/userlist/UserList'
import UserListFilter from '~/components/account/userlist/UserListFilter'
import UserListForm from '~/components/account/userlist/UserListForm'

import RoleListSearch from '~/components/account/roles/RoleListSearch'
import RoleList from '~/components/account/roles/RoleList'
import RoleListFilter from '~/components/account/roles/RoleListFilter'
import RoleListForm from '~/components/account/roles/RoleListForm'
import RoleRights from '~/components/account/roles/RoleRights'
import RoleMenuRights from '~/components/account/roles/RoleMenuRights'

import AccessTypesSearch from '~/components/account/accesstypes/AccessTypesSearch'
import AccessTypesList from '~/components/account/accesstypes/AccessTypesList'
import AccessTypesFilter from '~/components/account/accesstypes/AccessTypesFilter'
import AccessTypesForm from '~/components/account/accesstypes/AccessTypesForm'
import AccessTypesRights from '~/components/account/accesstypes/AccessTypesRights'
import AccessTypesMenuRights from '~/components/account/accesstypes/AccessTypesMenuRights'

import DepartmentSearch from '~/components/account/departments/DepartmentSearch'
import DepartmentList from '~/components/account/departments/DepartmentList'
import DepartmentFilter from '~/components/account/departments/DepartmentFilter'
import DepartmentForm from '~/components/account/departments/DepartmentForm'

import PositionSearch from '~/components/account/positions/PositionSearch'
import PositionList from '~/components/account/positions/PositionList'
import PositionFilter from '~/components/account/positions/PositionFilter'
import PositionForm from '~/components/account/positions/PositionForm'
import PositionMembers from '~/components/account/positions/PositionMembers'

import UserGroupsSearch from '~/components/account/usergroups/UserGroupsSearch'
import UserGroupsList from '~/components/account/usergroups/UserGroupsList'
import UserGroupsFilter from '~/components/account/usergroups/UserGroupsFilter'
import UserGroupsForm from '~/components/account/usergroups/UserGroupsForm'
import UserGroupsMember from '~/components/account/usergroups/UserGroupsMember'

import UserProfile from '~/components/account/UserProfile'

import CategorySearch from '~/components/manage/categories/CategorySearch'
import CategoryList from '~/components/manage/categories/CategoryList'
import CategoryFilter from '~/components/manage/categories/CategoryFilter'
import CategoryForm from '~/components/manage/categories/CategoryForm'

import RecordTypeSearch from '~/components/manage/records/RecordTypeSearch'
import RecordTypeList from '~/components/manage/records/RecordTypeList'
import RecordTypeFilter from '~/components/manage/records/RecordTypeFilter'
import RecordTypeForm from '~/components/manage/records/RecordTypeForm'

const components = {
  Toolbar,
  Navbar,
  Snackbar,
  CustomSnackbar,
  GlobalDashboardCard,
  UrgentEmails,
  AdvancedSearch,
  ComposeForm,
  ComposeAttachments,
  ComposeLinks,
  SentSearch,
  SentList,
  SentFilter,
  SentPreview,
  InboxSearch,
  InboxList,
  InboxFilter,
  InboxPreview,
  DraftSearch,
  DraftList,
  DraftFilter,
  RouteForm,
  RouteAttachments,
  RouteLinks,
  RouteComments,
  UserListSearch,
  UserList,
  UserListFilter,
  UserListForm,
  RoleListSearch,
  RoleList,
  RoleListFilter,
  RoleListForm,
  RoleRights,
  RoleMenuRights,
  AccessTypesSearch,
  AccessTypesList,
  AccessTypesFilter,
  AccessTypesForm,
  AccessTypesRights,
  AccessTypesMenuRights,
  DepartmentSearch,
  DepartmentList,
  DepartmentFilter,
  DepartmentForm,
  PositionSearch,
  PositionList,
  PositionFilter,
  PositionForm,
  PositionMembers,
  UserGroupsSearch,
  UserGroupsList,
  UserGroupsFilter,
  UserGroupsForm,
  UserGroupsMember,
  UserProfile,
  CategorySearch,
  CategoryList,
  CategoryFilter,
  CategoryForm,
  RecordTypeSearch,
  RecordTypeList,
  RecordTypeFilter,
  RecordTypeForm
}

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})
