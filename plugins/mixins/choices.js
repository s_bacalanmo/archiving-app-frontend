export default {
  data: () => ({
    status: [
      'Active',
      'Inactive'
    ],
    yesno: [
      'Yes',
      'No'
    ]
  })
}
