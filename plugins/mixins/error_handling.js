import _ from 'lodash'
import Vue from 'vue'

export default {
  data: () => ({
    error: {
      profile: {}
    },
    carderror: {},
    errorCharges: {}
  }),
  methods: {
    setError (payload) {
      _.forEach(payload, (value, key) => {
        this.$set(this.error, key, value)
        // console.log(key + ' - ' + value)
        if (_.isUndefined(value.length)) {
          _.forEach(value, (innervalue, innerkey) => {
            this.$set(this.error.profile, innerkey, innervalue)
          })
        }
        // console.log(this.error)
      })
    },
    setChargesError (payload, name) {
      let error = {}
      _.forEach(payload, (value, key) => {
        this.$set(error, key, value)
      })
      this.$set(this.errorCharges, name, error)
    },
    setCardError (payload) {
      _.forEach(payload, (value) => {
        /*
              NOTE: Expression that will check if value is object
            */
        let regex = /\./
        if (regex.test(value.param)) {
          let splits = _.split(value.param, '.')
          Vue.set(this.carderror, splits[1], value.message)
        } else {
          Vue.set(this.carderror, value.param, value.message)
        }
      })
    },
    resetError () {
      this.error = { profile: {} }
      this.carderror = {}
    },
    resetChargesError (payload) {
      this.$set(this.errorCharges, payload, {})
    }
  }
}
