export default {
  data: () => ({
    crms: {
      logo: require('@/assets/images/logo.png'),
      bg: require('@/assets/images/login_bg.jpg'),
      caro_logo: require('@/assets/images/CARO-Logo.png')
    }
  })
}
